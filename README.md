# ansible-interview-test

## Objective

Create an Ansible playbook that accomplishes the following:

* Install apache 2.4
* Install mariadb
* Configure apache to serve the index.html in this repository
* Configure mariadb and apache to start when the server is started
* Open ports 80 and 443 in firewalld
* Set selinux to enforcing and persist it

All this should be done by the user ansible (create it beforehand)

When you are done, create a merge request it to this repository.


**BEGIN**:



1.  The first step is to install ansible in ansible-manager host.
We are using CentOS 7 for this get started guide.

`yum install ansible`


2.  Then, we have to chose the user(s) which will SSH to the node(s).
It was asked to use the user 'ansible'.

After the user has been created in the node(s) test-machine(s), we will set up a connection between theese and the ansible-manager. 
This set up will be done using the default ssh shared keys authentication mechanism.


3. Create the key 
```
./ssh-keygen -t rsa -C "user@ansible-test1"
	-> When requested, give a path (example): /root/.ssh/ansible-test1
```


4.  Copy the key to ansible-test1 node
`./ssh-copy-id -i /root/.ssh/ansible-test1.pub user@ansible-test1`

There are ways to make sure the connection is established among the machines involved, we will skip directly to the core of this project.



5.  'cd' to ansible folder (where the playbook is) and run the playbook:
`ansible-playbook -i hosts playbook.yml --ask-become-pass`

A few notes.

* There is not need to implicity call 'hosts', as long as it's in use the default hosts in /etc/ansible folder.
*   The playbook (or better, the tasks inside) contains instructions to be ran as the root user. For security reason I would not store the password in the playbook file itself.
*   The playbook uses the directive 'become_user: root' but it could also be removed as the 'become: true' directive will already point to the root user.
*   index.html has been replaced with index.php in roles/apache2/files