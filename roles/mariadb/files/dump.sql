DROP TABLE IF EXISTS test_table;

CREATE TABLE test_table (
  message varchar(255) NULL default ''
);

INSERT INTO test_table (message) VALUES('Hello guys at Cloudwerkstatt!');